/* -*- Mode: C; Character-encoding: utf-8; -*- */

/* raptor.c
   This implements Kno bindings to raptor.
   Copyright (C) 2007-2019 beingmeta, inc.
   Copyright (C) 2020-2024 Kenneth Haase (ken.haase@alum.mit.edu)
*/

#ifndef _FILEINFO
#define _FILEINFO __FILE__
#endif

#define U8_INLINE_IO 1
#define KNO_DEFINE_GETOPT 1
#define KNO_SOURCE 1

#include "kno/knosource.h"
#include "kno/lisp.h"
#include "kno/numbers.h"
#include "kno/eval.h"
#include "kno/sequences.h"
#include "kno/texttools.h"
#include "kno/cprims.h"

#include <libu8/libu8.h>
#include <libu8/u8printf.h>
#include <libu8/u8crypto.h>

#include <raptor2/raptor2.h>

static lispval raptor_module;
KNO_EXPORT int kno_initialize_raptor(void) KNO_LIBINIT_FN;
static int raptor_initialized = 0;

static raptor_world *default_world;

U8_STATIC_MUTEX(raptor_init_lock);
KNO_EXPORT int kno_initialize_raptor(void) KNO_LIBINIT_FN;

#if KNO_MAJOR_VERSION >= 2309
#define make_slotmap(space,len,inits) \
  (kno_init_slotmap(NULL,space,KNO_SLOTMAP_SORTED,len,inits))
#else
#define make_slotmap(space,len,inits) (kno_make_slotmap(space,len,inits))
#endif

lispval RAPTOR_WORLD_TAG, RAPTOR_PARSER_TAG, RAPTOR_URI_TAG;
lispval RAPTOR_NAMESPACE_TAG, RAPTOR_STATEMENT_TAG, RAPTOR_TERM_TAG;

#define RAPTOR_WORLD_TYPE (0x2443a8583a95f874)
#define RAPTOR_PARSER_TYPE (0x2443a85808138641)
#define RAPTOR_URI_TYPE (0x2443a8581e7ff521)
#define RAPTOR_NAMESPACE_TYPE (0x2443a8580508293d)
#define RAPTOR_STATEMENT_TYPE (0x2443a858737b8ddc)
#define RAPTOR_TERM_TYPE (0x2443a8586ceaf087)

struct KNO_TYPEINFO *raptor_world_typeinfo=NULL,
  *raptor_parser_typeinfo=NULL,
  *raptor_uri_typeinfo=NULL,
  *raptor_namespace_typeinfo=NULL,
  *raptor_statement_typeinfo=NULL,
  *raptor_term_typeinfo=NULL;

lispval raptor_uri_term, raptor_literal_term;
lispval raptor_blank_term, raptor_unknown_term;

struct KNO_SLOTMAP raptor_options, raptor_domains;

static unsigned char *_memdup(const unsigned char *data,int len)
{
  unsigned char *duplicate = u8_alloc_n(len,unsigned char);
  memcpy(duplicate,data,len);
  return duplicate;
}

/* ENUMs */

static void _add_to_enum_table(struct KNO_SLOTMAP *table,u8_string name,int val)
{
  lispval symbol = KNO_VOID;
  int option_prefix_len=strlen("RAPTOR_OPTION_");
  int domain_prefix_len=strlen("RAPTOR_DOMAIN_");
  if (strncmp(name,"RAPTOR_OPTION_",option_prefix_len)==0)
    symbol=kno_getsym(name+option_prefix_len);
  else if (strncmp(name,"RAPTOR_DOMAIN_",option_prefix_len)==0)
    symbol=kno_getsym(name+domain_prefix_len);
  else return;
  kno_slotmap_store(table,symbol,KNO_INT(val));
}

#define ADD_DOMAIN_ENUM(name) \
  _add_to_enum_table(&raptor_domains,# name,name)
#define ADD_OPTION_ENUM(name) \
  _add_to_enum_table(&raptor_options,# name,name)

void init_raptor_enums()
{
  kno_init_slotmap(&raptor_domains,16,KNO_SLOTMAP_SORTED,0,NULL);
  kno_init_slotmap(&raptor_options,64,KNO_SLOTMAP_SORTED,0,NULL);
  ADD_DOMAIN_ENUM(RAPTOR_DOMAIN_NONE);
  ADD_DOMAIN_ENUM(RAPTOR_DOMAIN_IOSTREAM);
  ADD_DOMAIN_ENUM(RAPTOR_DOMAIN_NAMESPACE);
  ADD_DOMAIN_ENUM(RAPTOR_DOMAIN_PARSER);
  ADD_DOMAIN_ENUM(RAPTOR_DOMAIN_QNAME);
  ADD_DOMAIN_ENUM(RAPTOR_DOMAIN_SAX2);
  ADD_DOMAIN_ENUM(RAPTOR_DOMAIN_SERIALIZER);
  ADD_DOMAIN_ENUM(RAPTOR_DOMAIN_TERM);
  ADD_DOMAIN_ENUM(RAPTOR_DOMAIN_TURTLE_WRITER);
  ADD_DOMAIN_ENUM(RAPTOR_DOMAIN_URI);
  ADD_DOMAIN_ENUM(RAPTOR_DOMAIN_WORLD);
  ADD_DOMAIN_ENUM(RAPTOR_DOMAIN_WWW);
  ADD_DOMAIN_ENUM(RAPTOR_DOMAIN_XML_WRITER);

  ADD_OPTION_ENUM(RAPTOR_OPTION_SCANNING);
  ADD_OPTION_ENUM(RAPTOR_OPTION_ALLOW_NON_NS_ATTRIBUTES);
  ADD_OPTION_ENUM(RAPTOR_OPTION_ALLOW_OTHER_PARSETYPES);
  ADD_OPTION_ENUM(RAPTOR_OPTION_ALLOW_BAGID);
  ADD_OPTION_ENUM(RAPTOR_OPTION_ALLOW_RDF_TYPE_RDF_LIST);
  ADD_OPTION_ENUM(RAPTOR_OPTION_NORMALIZE_LANGUAGE);
  ADD_OPTION_ENUM(RAPTOR_OPTION_NON_NFC_FATAL);
  ADD_OPTION_ENUM(RAPTOR_OPTION_WARN_OTHER_PARSETYPES);
  ADD_OPTION_ENUM(RAPTOR_OPTION_CHECK_RDF_ID);
  ADD_OPTION_ENUM(RAPTOR_OPTION_RELATIVE_URIS);
  ADD_OPTION_ENUM(RAPTOR_OPTION_WRITER_AUTO_INDENT);
  ADD_OPTION_ENUM(RAPTOR_OPTION_WRITER_AUTO_EMPTY);
  ADD_OPTION_ENUM(RAPTOR_OPTION_WRITER_INDENT_WIDTH);
  ADD_OPTION_ENUM(RAPTOR_OPTION_WRITER_XML_VERSION);
  ADD_OPTION_ENUM(RAPTOR_OPTION_WRITER_XML_DECLARATION);
  ADD_OPTION_ENUM(RAPTOR_OPTION_NO_NET);
  ADD_OPTION_ENUM(RAPTOR_OPTION_RESOURCE_BORDER);
  ADD_OPTION_ENUM(RAPTOR_OPTION_LITERAL_BORDER);
  ADD_OPTION_ENUM(RAPTOR_OPTION_BNODE_BORDER);
  ADD_OPTION_ENUM(RAPTOR_OPTION_RESOURCE_FILL);
  ADD_OPTION_ENUM(RAPTOR_OPTION_LITERAL_FILL);
  ADD_OPTION_ENUM(RAPTOR_OPTION_BNODE_FILL);
  ADD_OPTION_ENUM(RAPTOR_OPTION_HTML_TAG_SOUP);
  ADD_OPTION_ENUM(RAPTOR_OPTION_MICROFORMATS);
  ADD_OPTION_ENUM(RAPTOR_OPTION_HTML_LINK);
  ADD_OPTION_ENUM(RAPTOR_OPTION_WWW_TIMEOUT);
  ADD_OPTION_ENUM(RAPTOR_OPTION_WRITE_BASE_URI);
  ADD_OPTION_ENUM(RAPTOR_OPTION_WWW_HTTP_CACHE_CONTROL);
  ADD_OPTION_ENUM(RAPTOR_OPTION_WWW_HTTP_USER_AGENT);
  ADD_OPTION_ENUM(RAPTOR_OPTION_JSON_CALLBACK);
  ADD_OPTION_ENUM(RAPTOR_OPTION_JSON_EXTRA_DATA);
  ADD_OPTION_ENUM(RAPTOR_OPTION_RSS_TRIPLES);
  ADD_OPTION_ENUM(RAPTOR_OPTION_ATOM_ENTRY_URI);
  ADD_OPTION_ENUM(RAPTOR_OPTION_PREFIX_ELEMENTS);
  ADD_OPTION_ENUM(RAPTOR_OPTION_STRICT);
  ADD_OPTION_ENUM(RAPTOR_OPTION_WWW_CERT_FILENAME);
  ADD_OPTION_ENUM(RAPTOR_OPTION_WWW_CERT_TYPE);
  ADD_OPTION_ENUM(RAPTOR_OPTION_WWW_CERT_PASSPHRASE);
  ADD_OPTION_ENUM(RAPTOR_OPTION_NO_FILE);
  ADD_OPTION_ENUM(RAPTOR_OPTION_WWW_SSL_VERIFY_PEER);
  ADD_OPTION_ENUM(RAPTOR_OPTION_WWW_SSL_VERIFY_HOST);
  ADD_OPTION_ENUM(RAPTOR_OPTION_LOAD_EXTERNAL_ENTITIES);
}

/* Printers */

int unparse_term(u8_output out,lispval obj,int depth,kno_printopts propts)
{
  raptor_term *term = (raptor_term *) KNO_RAWPTR_VALUE(obj);
  switch (term->type) {
  case RAPTOR_TERM_TYPE_URI: {
    raptor_uri *uri = term->value.uri;
    u8_string uri_text = raptor_uri_to_string(uri);
    u8_printf(out,"#<RDF/URI '%s'>",uri_text);
    return 1;}
  case RAPTOR_TERM_TYPE_LITERAL: {
    raptor_term_literal_value litval = term->value.literal;
    u8_string string = litval.string;
    u8_string langid = litval.language;
    u8_string type_text = raptor_uri_to_string(litval.datatype);
    if ( (string) && (langid) && (type_text) )
      u8_printf(out,"#<RDF/LITERAL '%s' langid=%s type=%s>",
                string,langid,type_text);
    else if ( (string) && (type_text) )
      u8_printf(out,"#<RDF/LITERAL '%s' type=%s>",
                string,langid,type_text);
    else if ( (string) && (langid) )
      u8_printf(out,"#<RDF/LITERAL '%s' lang=%s>",string,langid);
    else if (string)
      u8_printf(out,"#<RDF/LITERAL '%s'>",string);
    else
      return 1;}
  case RAPTOR_TERM_TYPE_BLANK: {
    raptor_term_blank_value blank = term->value.blank;
    u8_printf(out,"#<RDF/BLANK '%s'>",blank.string);
    return 1;}
  case RAPTOR_TERM_TYPE_UNKNOWN: {}
  }
  return 1;
}

static lispval blank_node_symbol;

lispval term2lisp(raptor_term *term)
{
  switch (term->type) {
  case RAPTOR_TERM_TYPE_URI: {
    ssize_t len=-1;
    raptor_uri *uri = term->value.uri;
    u8_string uri_text = raptor_uri_to_string(uri);
    lispval string = kno_mkstring(uri_text);
    raptor_free_memory((void *)uri_text);
    return string;}
  case RAPTOR_TERM_TYPE_LITERAL: {
    lispval result = KNO_VOID;
    raptor_term_literal_value litval = term->value.literal;
    u8_string string = litval.string;
    u8_string langid = litval.language;
    u8_string type_text = raptor_uri_to_string(litval.datatype);
    u8_string result_string = NULL;
    if ( (string) && (langid) && (type_text) )
      result_string=u8_mkstring("%s$%s<%s>",langid,string,type_text);
    else if ( (string) && (type_text) )
      result_string=u8_mkstring("%s<%s>",string,type_text);
    else if ( (string) && (langid) )
      result_string=u8_mkstring("%s$%s",langid,string);
    else if (string)
      result_string=u8_strdup(string);
    else NO_ELSE;
    if (result_string)
      result=kno_wrapstring(result_string);
    else result=KNO_FALSE;
    return result;}
  case RAPTOR_TERM_TYPE_BLANK: {
    raptor_term_blank_value blank = term->value.blank;
    lispval nodeid = kno_make_string(NULL,blank.string_len,blank.string);
    return kno_init_compound(NULL,blank_node_symbol,0,1,nodeid);}
  case RAPTOR_TERM_TYPE_UNKNOWN: {
    return KNO_TRUE;}
  }
}

void handle_statement(void *data,raptor_statement *stmt)
{
  lispval fn = (lispval) data;
  int n = 0;
  lispval args[4];
  if (stmt->subject) args[n++]=term2lisp(stmt->subject);
  if (stmt->predicate) args[n++]=term2lisp(stmt->predicate);
  if (stmt->object) args[n++]=term2lisp(stmt->object);
  if (stmt->graph) args[n++]=term2lisp(stmt->graph);
  lispval result = kno_apply(fn,n,args);
  kno_decref(result);
}

lispval handle_uri(lispval path,lispval fn)
{
  raptor_parser *parser = raptor_new_parser(default_world,"temp");
  raptor_uri *uri = raptor_new_uri(default_world,CSTRING(path));
  raptor_parser_set_statement_handler(parser,(void *)fn,handle_statement);
  int rv = raptor_parser_parse_uri(parser,uri,NULL);
  if (rv<0) return KNO_FALSE; else return KNO_VOID;
}

/* Setup */

KNO_EXPORT int kno_initialize_raptor()
{
  u8_init_static_mutex(raptor_init_lock);
  if (raptor_initialized) return 0;
  u8_lock_mutex(&raptor_init_lock);
  if (raptor_initialized) {
    u8_unlock_mutex(&raptor_init_lock);
    return 0;}
  KNO_INITIALIZE();
  raptor_initialized = 1;

  init_raptor_enums();
  raptor_uri_term=kno_intern("raptoruri");
  raptor_literal_term=kno_intern("raptorliteral");
  raptor_blank_term=kno_intern("raptorblanknode");
  raptor_unknown_term=kno_intern("raptorunknowntermtype");
  blank_node_symbol=kno_intern("%blank");

  default_world=raptor_new_world();

  raptor_module = kno_new_cmodule("raptor",(0),kno_initialize_raptor);

  kno_register_raw_type("raptor_world",RAPTOR_WORLD_TYPE,
                        "A Raptor library world object",
                        &RAPTOR_WORLD_TAG,&raptor_world_typeinfo,
                        raptor_module);
  kno_register_raw_type("raptor_parser",RAPTOR_PARSER_TYPE,
                        "A Raptor library parser object",
                        &RAPTOR_PARSER_TAG,&raptor_parser_typeinfo,
                        raptor_module);
  kno_register_raw_type("raptor_uri",RAPTOR_URI_TYPE,
                        "A Raptor library uri object",
                        &RAPTOR_URI_TAG,&raptor_uri_typeinfo,
                        raptor_module);
  kno_register_raw_type("raptor_namespace",RAPTOR_NAMESPACE_TYPE,
                        "A Raptor library namespace object",
                        &RAPTOR_NAMESPACE_TAG,&raptor_namespace_typeinfo,
                        raptor_module);
  kno_register_raw_type("raptor_statement",RAPTOR_STATEMENT_TYPE,
                        "A Raptor library statement object",
                        &RAPTOR_STATEMENT_TAG,&raptor_statement_typeinfo,
                        raptor_module);
  kno_register_raw_type("raptor_term",RAPTOR_TERM_TYPE,
                        "A Raptor library term object",
                        &RAPTOR_TERM_TAG,&raptor_term_typeinfo,
                        raptor_module);

  link_local_cprims();

  kno_finish_module(raptor_module);

  u8_register_source_file(_FILEINFO);

  u8_unlock_mutex(&raptor_init_lock);
  return 1;
}

static void link_local_cprims()
{

}
